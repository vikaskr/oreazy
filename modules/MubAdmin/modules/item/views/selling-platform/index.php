<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\MubAdmin\modules\item\models\SellingPlatformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Selling Platforms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selling-platform-index">
 <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="header">
                    <div class="row">
                    <div class="col-md-6">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <div class="col-md-6 text-right">
                    <p>
                        <?= Html::a('Create Selling Platform', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                    </div>
                    </div>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'mub_user_id',
                            'name',
                            'slug',
                            'image',
                            //'status',
                            //'created_at',
                            //'updated_at',
                            //'del_status',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
               </div>
            </div>
        </div>
    </div>
</div>


