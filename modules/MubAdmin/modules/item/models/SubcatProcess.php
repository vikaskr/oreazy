<?php 
namespace app\modules\MubAdmin\modules\item\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class SubcatProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $subcat = new Subcat();
        $subcat->scenario = 'create';
        $this->models = [
            'subcat' => $subcat
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $category = new Category();
        $where = ['del_status' => '0','status' => 'active'];
        $allcategory = $category->getAll('name',$where);
        return [
            'allcategory' => $allcategory,
        ];
    }

    public function getRelatedModels($model)
    {
        $subcat = $model;
        $this->relatedModels = [
            'subcat' => $subcat
        ];
        return $this->relatedModels;
    }

    public function saveImage($subcat)
    {
        $subcat->image = UploadedFile::getInstance($subcat,'image');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($subcat,'image');
        return $subcat;
    }

    public function saveSubcat($subcat)
    {
        $image = UploadedFile::getInstance($subcat,'image');
        if($image)
        {
            $this->SaveImage($subcat);
        }
        else 
        {  
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $subcatModel = new Subcat();
                $image = $subcatModel->findOne($subcat->id)->image;
                $subcat->image = $image;
            }
        }
        $subcat->slug = StringHelper::generateSlug($subcat->name);
        $mubUserId = \app\models\User::getMubUserId();
        $subcat->mub_user_id = $mubUserId;
        return ($subcat->save()) ? $subcat->id : p($subcat->getErrors());
    }

    public function saveData($data)
    {
        if(isset($data['subcat']))
        {
        try {
            $catId = $this->saveSubcat($data['subcat']);
            return ($catId) ? $catId : false;  
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        }
        throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
