<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $sellingplatform app\modules\MubAdmin\modules\item\sellingplatforms\SellingPlatform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="selling-platform-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row"> 
        <div class="col-md-6"><?= $form->field($sellingplatform, 'name')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-6">
    <?= $form->field($sellingplatform, 'slug')->textInput(['maxlength' => true]) ?>
        </div></div>
        <div class="row"> 
                <div class="col-md-6">
            <?= $form->field($sellingplatform, 'image')->fileInput(['class' => 'border-input form-control']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($sellingplatform, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
        </div></div>
   <div class="row"> 
        <div class="col-md-12 text-center">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
   </div>
    <?php ActiveForm::end(); ?>

</div>
