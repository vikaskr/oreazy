<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $sellingplatform app\modules\MubAdmin\modules\item\sellingplatforms\SellingPlatform */

$this->title = 'Update Selling Platform: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Selling Platforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $sellingplatform->name, 'url' => ['view', 'id' => $sellingplatform->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="selling-platform-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'sellingplatform' => $sellingplatform,
    ]) ?>

</div>
