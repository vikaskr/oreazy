<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\modules\MubAdmin\modules\item\models\SellingPlatform;
use app\modules\MubAdmin\modules\item\models\SellingPlatformSearch;
use app\modules\MubAdmin\modules\item\models\SellingPlatformProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SellingPlatformController implements the CRUD actions for SellingPlatform model.
 */
class SellingPlatformController extends MubController
{
      public function getPrimaryModel()
   {
        return new SellingPlatform();
   }

   public function getProcessModel()
   {
        return new SellingPlatformProcess();
   }

   public function getSearchModel()
   {
        return new SellingPlatformSearch();
   }

}
