<?php

namespace app\modules\MubAdmin\modules\item;

/**
 * item module definition class
 */
class Item extends \app\modules\MubAdmin\MubAdmin
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\MubAdmin\modules\item\controllers';
    public $defaultRoute = 'product';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
