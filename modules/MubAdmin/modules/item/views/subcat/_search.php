<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $subcat app\modules\MubAdmin\modules\item\models\SubcatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subcat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($subcat, 'id') ?>

    <?= $form->field($subcat, 'item_category_id') ?>

    <?= $form->field($subcat, 'name') ?>

    <?= $form->field($subcat, 'type') ?>

    <?= $form->field($subcat, 'slug') ?>

    <?php // echo $form->field($subcat, 'priority') ?>

    <?php // echo $form->field($subcat, 'description') ?>

    <?php // echo $form->field($subcat, 'image') ?>

    <?php // echo $form->field($subcat, 'status') ?>

    <?php // echo $form->field($subcat, 'created_at') ?>

    <?php // echo $form->field($subcat, 'updated_at') ?>

    <?php // echo $form->field($subcat, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
