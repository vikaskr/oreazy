<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $brand app\modules\MubAdmin\modules\item\brands\BrandSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($brand, 'id') ?>

    <?= $form->field($brand, 'manufacture_id') ?>

    <?= $form->field($brand, 'name') ?>

    <?= $form->field($brand, 'slug') ?>

    <?= $form->field($brand, 'logo') ?>

    <?php // echo $form->field($brand, 'status') ?>

    <?php // echo $form->field($brand, 'created_at') ?>

    <?php // echo $form->field($brand, 'updated_at') ?>

    <?php // echo $form->field($brand, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
