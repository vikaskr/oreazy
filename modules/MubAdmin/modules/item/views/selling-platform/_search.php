<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $sellingplatform app\modules\MubAdmin\modules\item\models\SellingPlatformSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="selling-platform-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($sellingplatform, 'id') ?>

    <?= $form->field($sellingplatform, 'mub_user_id') ?>

    <?= $form->field($sellingplatform, 'name') ?>

    <?= $form->field($sellingplatform, 'slug') ?>

    <?= $form->field($sellingplatform, 'image') ?>

    <?php // echo $form->field($sellingplatform, 'status') ?>

    <?php // echo $form->field($sellingplatform, 'created_at') ?>

    <?php // echo $form->field($sellingplatform, 'updated_at') ?>

    <?php // echo $form->field($sellingplatform, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
