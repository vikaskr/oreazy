<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\item\categorys\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

<?php $form = ActiveForm::begin(['enableAjaxValidation' => true,'validationUrl' => ['../../../validate/catagory-validate'],'options' => ['method' => 'POST','data-pjax' => true]]); ?>

   <div class="row"> 
        <div class="col-md-6"><?= $form->field($category, 'name')->textInput(['maxlength' => true,'class'=>'border-input form-control']) ?></div>

        <div class="col-md-6"><?= $form->field($category, 'type')->dropDownList([ 'shiping' => 'Shiping', 'item' => 'Item', 'planning' => 'Planning', ], ['prompt' => ' Select Category Type', 'id'=>'show_value','class'=>'border-input form-control']) ?></div>
    </div>

    <div class="row"> 
        <div class="col-md-12"><?= $form->field($category, 'description')->textarea(['rows' => 6,'class'=>'border-input form-control']) ?></div>
    </div>
    <div class="row"> 
        <div class="col-md-12">
            <?= $form->field($category, 'image')->fileInput(['class' => 'border-input form-control']); ?></div>
        <!-- <div class="col-md-5"><?php //$form->field($category, 'priority')->textInput(['maxlength' => true]) ?></div> -->
    </div>
    <?php if(\Yii::$app->controller->action->id == 'update'){?>
    <div class="row text-center">
        <div class="col-md-6">
            <img src="/<?= $category->image;?>" class="img-responsive">
        </div>
    </div>
    <?php }?>
    <div class="row"> 
        <div class="col-md-12 text-center">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end();?>
</div>