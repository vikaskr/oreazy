<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\SellingPlatform */

$this->title = 'Create Selling Platform';
$this->params['breadcrumbs'][] = ['label' => 'Selling Platforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selling-platform-create">
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
    		<div class="card">
    			<div class="header">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'sellingplatform' => $sellingplatform,
    ]) ?>
</div>
</div>
</div>
</div>
</div>
