<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\modules\MubAdmin\modules\item\models\Brand;
use app\modules\MubAdmin\modules\item\models\BrandSearch;
use app\modules\MubAdmin\modules\item\models\BrandProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BrandController implements the CRUD actions for Brand model.
 */
class BrandController extends MubController
{
   public function getPrimaryModel()
   {
        return new Brand();
   }

   public function getProcessModel()
   {
        return new BrandProcess();
   }

   public function getSearchModel()
   {
        return new BrandSearch();
   }
}