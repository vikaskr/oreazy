<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Subcat */

$this->title = $subcat->name;
$this->params['breadcrumbs'][] = ['label' => 'Subcats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subcat-view">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="header">

                    <p><?= Html::encode($this->title) ?></p>

                    <p>
                        <?= Html::a('Update', ['update', 'id' => $subcat->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $subcat->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>

                    <?= DetailView::widget([
                        'model' => $subcat,
                        'attributes' => [
                            'id',
                            'item_category_id',
                            'name',
                            'slug',
                            'priority',
                            'description:ntext',
                            'image',
                            'status',
                            'created_at',
                            'updated_at',
                            'del_status',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
