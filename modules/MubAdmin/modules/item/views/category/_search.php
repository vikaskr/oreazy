<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\item\categorys\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($category, 'id') ?>

    <?= $form->field($category, 'name') ?>

    <?= $form->field($category, 'type') ?>

    <?= $form->field($category, 'slug') ?>

    <?= $form->field($category, 'priority') ?>

    <?php // echo $form->field($category, 'description') ?>

    <?php // echo $form->field($category, 'image') ?>

    <?php // echo $form->field($category, 'status') ?>

    <?php // echo $form->field($category, 'created_at') ?>

    <?php // echo $form->field($category, 'updated_at') ?>

    <?php // echo $form->field($category, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
