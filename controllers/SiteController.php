<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

       $this->view->params['page'] = 'home';
       return $this->redirect('/mub-admin');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'admin';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionCategory()
    {
        return $this->render('category');
    }

    public function actionAllcategory()
    {
        $category = new \app\modules\MubAdmin\modules\furniture\models\Category();
        return $this->render('allcategory',['category' => $category]);
    }

    public function actionProduct($id)
    {
       $singleProduct = new \app\modules\MubAdmin\modules\furniture\models\Product();
       $products = $singleProduct::find()->where(['id' => $id,'del_status' => '0'])->one();

        return $this->render('product',[
            'products' => $products]);
    }

    public function actionAllproduct()
    {
       $product = new \app\modules\MubAdmin\modules\furniture\models\Product();
       $category = new \app\modules\MubAdmin\modules\furniture\models\Category();
       return $this->render('allproduct',['product' => $product,'category' => $category]);
    }

    public function actionAddtocart($id)
    {
        $cart = new ShoppingCart();

        $model = Product::findOne($id);
        if ($model) {
            $cart->put($model, 1);
            return $this->render('addtocart', [
            'cart' => $cart,
        ]);
        }
        throw new NotFoundHttpException();
    }

}

