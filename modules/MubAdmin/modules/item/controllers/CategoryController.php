<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\modules\MubAdmin\modules\item\models\Category;
use app\modules\MubAdmin\modules\item\models\CategorySearch;
use app\modules\MubAdmin\modules\item\models\CategoryProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends MubController
{
     public function getPrimaryModel()
   {
        return new Category();
   }

   public function getProcessModel()
   {
        return new CategoryProcess();
   }

   public function getSearchModel()
   {
        return new CategorySearch();
   }

}
