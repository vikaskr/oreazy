<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Category */

$this->title = 'Create Category';
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

   <div class="row">
   		<div class="col-md-6 col-md-offset-3"> 
   			<div class="card">
   				<div class="header">	
	   				<?= Html::encode($this->title) ?>

				    <?= $this->render('_form', [
				        'category' => $category,
				    ]) ?>
			    </div>
	    	</div>
       	</div>
   </div>

</div>
