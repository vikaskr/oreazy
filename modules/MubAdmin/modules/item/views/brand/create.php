<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\Brand */

$this->title = 'Create Brand';
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-create">

    <div class="row">
    	<div class="col-md-5 col-md-offset-3">
    		<div class="card">
    			<div class="header">
		    		<p><?= Html::encode($this->title) ?></p>
		    	
			    	<?= $this->render('_form', [
			        'brand' => $brand,
			        'allmanufacture' => $allmanufacture,
			    	]) ?>
			    </div>
		    </div>
		</div>
	</div>
</div>
