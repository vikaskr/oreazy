<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $brand app\modules\MubAdmin\modules\item\brands\Brand */

$this->title = 'Update Brand: '.$brand->name;
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $brand->name, 'url' => ['view', 'id' => $brand->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
   		<div class="col-md-6 col-md-offset-3"> 
   			<div class="card">
   				<div class="header">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'brand' => $brand,
        'allmanufacture' => $allmanufacture,
    ]) ?>

</div>
