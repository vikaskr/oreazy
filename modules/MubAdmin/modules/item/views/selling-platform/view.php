<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\item\models\SellingPlatform */

$this->title = $sellingplatform->name;
$this->params['breadcrumbs'][] = ['label' => 'Selling Platforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="selling-platform-view">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="card">
                <div class="header">
                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Update', ['update', 'id' => $sellingplatform->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $sellingplatform->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                    <?= DetailView::widget([
                        'model' => $sellingplatform,
                        'attributes' => [
                            'id',
                            'mub_user_id',
                            'name',
                            'slug',
                            'image',
                            'status',
                            'created_at',
                            'updated_at',
                            'del_status',
                        ],
                    ]) ?>
                    <br/>
                </div>
            </div>
        </div>
    </div>
</div>
