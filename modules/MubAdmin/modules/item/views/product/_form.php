<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\ImageUploader;
use dosamigos\fileupload\FileUploadUI;
use app\modules\MubAdmin\modules\item\models\ProductImages;
$productImages = new ProductImages();
$mubUserId = \app\models\User::getMubUserId();
/* @var $this yii\web\View */
/* @var $product app\modules\MubAdmin\modules\item\products\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
    <div class="row">
    <div class="col-md-12">
        <h5><b>Product Images</b></h5>
        <?= FileUploadUI::widget([
        'model' => $productImages,
        'attribute' => 'thumbnail_url',
        'url' => ['product/product-image', 'userId' => $mubUserId],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 5000000,
            'load' => true
        ],
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                }',
            'fileuploadfail' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                }',
        ],
        ]); ?></div>
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6"><?= $form->field($product, 'model_number')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'item_package_quantity')->dropDownList([ 'quantity' => 'Quantity', 'quantity2' => 'Quantity2', ], ['prompt' => '']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'size')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'size_map')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'color')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'color_map')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'enclosure_material')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'manufacturer')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'title')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'manufacturer_part_number')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'brand')->textInput() ?></div>

    <div class="col-md-6"><?php echo $form->field($product, 'category')->dropDownList($allcategory,['class' => 'border-input form-control'])->label('CATEGORY'); ?>
    </div>
    <div class="col-md-6"><?php echo $form->field($product, 'sub_category')->dropDownList([],['class' => 'border-input form-control'])->label('SUB CATEGORY'); ?>
    </div>

    <div class="col-md-6"><?= $form->field($product, 'seller_sku')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'HSN_code')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'price')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_price')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'warranty_des')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'country')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'release_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_start_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'sale_end_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'restock_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'offer_release_date')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'condition_notes')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'leagle_notes')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'quantity')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'max_order_quantity')->textInput() ?></div>

    <div class="col-md-6"><?= $form->field($product, 'product_text_code')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'condition')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'handling_time')->textInput(['class' => 'border-input form-control']) ?></div>

    <div class="col-md-6"><?= $form->field($product, 'gift_wrap')->dropDownList([ 'yes' => 'Yes', 'no' => 'No', ], ['prompt' => '']) ?></div>

    <div class="col-md-12"><?= $form->field($product, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?></div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?></div>
    </div>

</div>
</div>
    <?php ActiveForm::end(); ?>

</div>
