<?php
namespace app\modules\MubAdmin\modules\item\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class CategoryProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $category = new Category();
        $category->scenario = 'create';
        $this->models = [
            'category' => $category
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $category = $model;
        $this->relatedModels = [
            'category' => $category
        ];
        return $this->relatedModels;
    }

    public function saveImage($category)
    {
        $category->image = UploadedFile::getInstance($category,'image');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($category,'image');
        return true;
    }

    public function saveCategory($category)
    {
        $image = UploadedFile::getInstance($category,'image');
        if($image)
        {
            $this->SaveImage($category);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $categoryModel = new Category();
                $image = $categoryModel->findOne($category->id)->image;
                $category->image = $image;
            }
        }
        $category->slug = StringHelper::generateSlug($category->name);
        $mubUserId = \app\models\User::getMubUserId();
        $category->mub_user_id = $mubUserId;
        return ($category->save()) ? $category->id : p($category->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['category']))
        {
        try {
        	$catId = $this->saveCategory($data['category']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
