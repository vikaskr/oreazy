<?php
namespace app\modules\MubAdmin\modules\item\models;


use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class SellingplatformProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $sellingplatform = new SellingPlatform();
        $this->models = [
            'sellingplatform' => $sellingplatform
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $sellingplatform = $model;
        $this->relatedModels = [
            'sellingplatform' => $sellingplatform
        ];
        return $this->relatedModels;
    }

    public function saveImage($sellingplatform)
    {
        $image = UploadedFile::getInstance($sellingplatform,'image');
        $sellingplatform->image = $image;
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($sellingplatform,'image');
        return true;
    }

    public function savesellingplatform($sellingplatform)
    {
        $image = UploadedFile::getInstance($sellingplatform,'image');
        if($image)
        {
            $this->SaveImage($sellingplatform);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $sellingplatformModel = new sellingplatform();
                $image = $sellingplatformModel->findOne($sellingplatform->id)->image;
                $sellingplatform->image = $image;
            }
        }       
        $sellingplatform->slug = StringHelper::generateSlug($sellingplatform->name);
        $mubUserId = \app\models\User::getMubUserId();
        $sellingplatform->mub_user_id = $mubUserId;
        return ($sellingplatform->save()) ? $sellingplatform->id : p($sellingplatform->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['sellingplatform']))
        {
        try {
        	$catId = $this->savesellingplatform($data['sellingplatform']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
