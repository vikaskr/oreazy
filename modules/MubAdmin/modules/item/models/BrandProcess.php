<?php 
namespace app\modules\MubAdmin\modules\item\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class BrandProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $brand = new Brand();
        $this->models = [
            'brand' => $brand
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $manufacture = new Manufacture();
        $where = ['del_status' => '0','status' => 'active'];
        $allmanufacture = $manufacture->getAll('name',$where);
        return [
            'allmanufacture' => $allmanufacture,
        ];
    }

    public function getRelatedModels($model)
    {
        $brand = $model;
        $this->relatedModels = [
            'brand' => $brand
        ];
        return $this->relatedModels;
    }

    public function saveImage($brand)
    {
        $brand->logo = UploadedFile::getInstance($brand,'logo');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($brand,'logo');
        return $brand;
    }


    public function savebrand($brand)
    {
        $image = UploadedFile::getInstance($brand,'logo');
        if($image)
        {
            $this->SaveImage($brand);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $brandModel = new Brand();
                $image = $brandModel->findOne($brand->id)->logo;
                $brand->logo = $image;
            }
        }   
        $brand->slug = StringHelper::generateSlug($brand->name);
        $mubUserId = \app\models\User::getMubUserId();
        $brand->mub_user_id = $mubUserId;
        return ($brand->save()) ? $brand->id : p($brand->getErrors());
    }

    public function saveData($data)
    {
        if(isset($data['brand']))
        {
        try {
            $catId = $this->saveBrand($data['brand']);
            return ($catId) ? $catId : false;  
            }
            catch (\Exception $e)
            {
                throw $e;
            }
        }
        throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
