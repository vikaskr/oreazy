<?php
namespace app\modules\MubAdmin\modules\item\models;

use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii;

class ManufactureProcess extends Model
{
	public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $manufacture = new Manufacture();
        $manufacture->scenario = 'create';
        $this->models = [
            'manufacture' => $manufacture
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return [];
    }

    public function getRelatedModels($model)
    {
        $manufacture = $model;
        $this->relatedModels = [
            'manufacture' => $manufacture
        ];
        return $this->relatedModels;
    }

    public function saveImage($manufacture)
    {
        $image = UploadedFile::getInstance($manufacture,'image');
        $manufacture->image = $image;
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($manufacture,'image');
        return true;
    }

    public function saveManufacture($manufacture)
    {
        $image = UploadedFile::getInstance($manufacture,'image');
        if($image)
        {
            $this->SaveImage($manufacture);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $manufactureModel = new Manufacture();
                $image = $manufactureModel->findOne($manufacture->id)->image;
                $manufacture->image = $image;
            }
        }       
        $manufacture->slug = StringHelper::generateSlug($manufacture->name);
        $mubUserId = \app\models\User::getMubUserId();
        $manufacture->mub_user_id = $mubUserId;
        return ($manufacture->save()) ? $manufacture->id : p($manufacture->getErrors());
    }

    public function saveData($data)
    {
    	if(isset($data['manufacture']))
        {
        try {
        	$catId = $this->saveManufacture($data['manufacture']);
        	return ($catId) ? $catId : false;  
        	}
        	catch (\Exception $e)
            {
                throw $e;
            }
        }
    	throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
    }
}
