<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $manufacture app\modules\MubAdmin\modules\item\manufactures\ManufactureSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manufacture-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($manufacture, 'id') ?>

    <?= $form->field($manufacture, 'name') ?>

    <?= $form->field($manufacture, 'slug') ?>

    <?= $form->field($manufacture, 'image') ?>

    <?= $form->field($manufacture, 'barcode') ?>

    <?php // echo $form->field($manufacture, 'status') ?>

    <?php // echo $form->field($manufacture, 'created_at') ?>

    <?php // echo $form->field($manufacture, 'updated_at') ?>

    <?php // echo $form->field($manufacture, 'del_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
