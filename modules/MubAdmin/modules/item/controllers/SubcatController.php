<?php

namespace app\modules\MubAdmin\modules\item\controllers;

use Yii;
use app\modules\MubAdmin\modules\item\models\Subcat;
use app\modules\MubAdmin\modules\item\models\SubcatSearch;
use app\modules\MubAdmin\modules\item\models\SubcatProcess;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubcatController implements the CRUD actions for Subcat model.
 */
class SubcatController extends MubController
{
     public function getPrimaryModel()
   {
        return new Subcat();
   }

   public function getProcessModel()
   {
        return new SubcatProcess();
   }

   public function getSearchModel()
   {
        return new SubcatSearch();
   }
}
